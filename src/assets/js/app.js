/**
 * @description
 * App js
 */

var scrollToAnchor = function(){
	$('.scroll-anchor a').click(function(e) {
	    $('.scroll-anchor').removeClass('active');
	    $(this).parent().addClass('active');

		e.preventDefault();
	    var the_id = $(this).attr('href');
	    var target = $(the_id);
	    if (target.length) {
	    	var targetoffset = target.offset().top;
	    	if (the_id == '#main-header') {
	    		targetoffset = 0;
	    	}
	    	console.log(targetoffset);
	        $('html,body').animate({
	            scrollTop: targetoffset
	        }, 1000);
	        return false;
	    }
	});

	// TOGGLE ON SCROLL
	function toggle_anchor_on_scroll(){

		$('.anchored-item').each(function(){
			var wScroll = $(window).scrollTop();

			if ($(this).offset().top < wScroll + $(window).height() - 50 && $(this).offset().top + $(this).height() > wScroll - $(window).height() + 50) {
				var theID = $(this).attr('id');
				
				$('.sidebar .scroll-anchor').removeClass('active');
				$('.sidebar a[href="#'+theID+'"]').parent().addClass('active');
	        }
			
		});
	}

	toggle_anchor_on_scroll();
	$(window).scroll(function(){
		toggle_anchor_on_scroll();
	});
}

var menuScroll = function(){

    $(window).scroll(function(){
    	if ($(window).scrollTop() > 100) {
    		$('.main-header').addClass('fixed');
    	}else{
    		$('.main-header').removeClass('fixed');
    	}

    	if ($(window).scrollTop() > 200) {
    		$('.main-header').addClass('to-bottom');
    	}else{
    		$('.main-header').removeClass('to-bottom');
    	}
    });
}

var topParallax = function(){
	var atualOffset = $('.panel-bg').offset().top;
	var wTop = $(window).scrollTop();

	$(window).scroll(function(){
		wTop = $(window).scrollTop();
		$('.panel-bg .panel').css("top",wTop/2+"px");
	});
}

var taxazero = function(){
	$('.taxa-trigger').click(function(){
		$('.taxa-zero').addClass('active');
		$('.taxa-zero .actions').fadeOut();
		// setTimeout(2000, $('.taxa-zero .content').slideDown());
		$('.taxa-zero .content').delay(500).slideDown();
	});

	if (window.location.hash == '#taxa-zero') {
		$('.taxa-zero').addClass('active');
		$('.taxa-zero .actions').fadeOut();
		// setTimeout(2000, $('.taxa-zero .content').slideDown());
		$('.taxa-zero .content').delay(500).slideDown();
	}
}

var topvaluesslide = function(){
	var element = $('.top-btn-vals .c-container');
	var initialVal = element.html();

	function doslide(){
		var wsize = $('body').width();

		if (wsize < 1200) {
			function duplicateval(){
				element.html($('.top-btn-vals .c-container').html()+initialVal);
				var Wleft = element.width() - wsize;

				function drag(){
					element.animate({
					    left: "-="+Wleft
					}, Wleft*16, "linear", function() {
						element.html($('.top-btn-vals .c-container').html()+initialVal);
						drag();
					});
				}

				drag();
			}

			duplicateval();

		}else{
			element.html(initialVal);
			element.attr('style', '');
		}
	}

	doslide();
	$(window).resize(function(){
		doslide();
	});
}


var featuresButton = function(){
	function showBtn(){
		var wZise = $('body').width();
		var wOffset = $(window).scrollTop();
		var wHeight = $(window).height();
		var featureoffset = $('#features').offset().top;
		var featuresHeight = $('#features').height();

		if (wZise < 769 && wOffset+wHeight-300 > featureoffset && wOffset+wHeight < featureoffset+featuresHeight ) {
			$('.features-mobile-btn').fadeIn(300);
		}else{
			$('.features-mobile-btn').fadeOut(300);
		}
	}

	showBtn();
	$(window).scroll(function(){
		showBtn();
	});
	$(window).resize(function(){
		showBtn();
	});
}


$(document).ready(function () {
    scrollToAnchor();
    topParallax();
    taxazero();
    topvaluesslide();
    featuresButton();
    menuScroll();
});

$(window).load(function(){
});



